package com.test.listener;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class MySpringContextLoaderListener extends ContextLoaderListener {
	
//	private Logger logger = Logger.getLogger(this.getClass());
	
	public void contextInitialized(ServletContextEvent event) {
		super.contextInitialized(event);
		MyServletContext.getInstance().setServletContext(event.getServletContext());
		
		MySpringWebApplicationContext.defaultWebApplicationContext = 
			WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
		
	}
}
		
