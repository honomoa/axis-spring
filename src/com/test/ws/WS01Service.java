package com.test.ws;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import org.springframework.remoting.jaxrpc.ServletEndpointSupport;

public class WS01Service extends ServletEndpointSupport implements IWS01 {
	private Logger logger = Logger.getLogger(this.getClass());
	private IWS01 ws01;

	@Override
	protected void onInit() throws ServiceException {
		// 在 Spring 容器中获取 Bean 的实例
		ws01 = (IWS01) getApplicationContext().getBean("ws01Bean");
	}

	@Override
	public void myMethod(String param) {
		// TODO Auto-generated method stub
		
	}

}
